public class DogRunner {
    
    public static void main(String[] args) {
        
        Dog d0 = new Dog("Jerry", "Corgi", 14, "small", true);
        d0.meetDog();

        Dog d1 = new Dog("Air Bud" , "Golden Retriver" , 17, "large", true);
        d1.meetDog();
    }

}
