public class DogBark {
    
    // Private Instance Variables
    private String name;
    private String breed;
    private boolean isAlive;

    public DogBark(String n, String b, boolean i) {
        name = n;
        breed = b;
        isAlive = i;
    }

    public void chooseBark() {
        if (isAlive == true) {
            happyBark();
        }
        else {
            sadBark();
        }
    }
    
    public void happyBark() {
        System.out.println("I am " + name + " and I am a " + breed);
        System.out.println("WOOF!");
    }

    public void sadBark() {
        System.out.println("woof....");
    }

}
