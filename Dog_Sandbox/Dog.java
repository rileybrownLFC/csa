// Class Header
public class Dog {

    // Private Instance Variables 
    private String name;
    private String breed;
    private int age;
    private String size;
    private boolean isAlive;

    // Constructor 
    public Dog(String n, String b, int a, String s, boolean i) {
        name = n;
        breed = b;
        age = a;
        size = s;
        isAlive = i;
    }

    // Methods

    // Main method which calls the other methods
    public void meetDog() {
        seperator();
        introduce();
        breed();
        age();
        size();
        isAlive();
        // picture(); (WIP)
        seperator();
    }
 
    // Introduces the name of the dog
    public void introduce() {
        System.out.println("Name: " + name);
    }

    // Tells you what breed the dog is
    public void breed() {
        System.out.println("Breed: " + breed);
    }

    // Tells you the name of the dog
    public void age() {
        System.out.println("Age: " + age);
    }

    // Tells you the size of the dog
    public void size() {
        System.out.println("Size: " + size);
    }

    // Checks to be sure the dog is alive
    public void isAlive() {
        System.out.println("Alive: " + isAlive);
    }

    // Picture of the dog (WIP)
    public void picture() {
        System.out.println("Picture:" );
    }

    // Line just to seperate dog information blurbs
    public void seperator() {
        System.out.println("-----------------------------------------------------------------------------");
    }

}